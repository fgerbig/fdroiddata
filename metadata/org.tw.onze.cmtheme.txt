Categories:System
License:GPLv3
Web Site:https://gitlab.com/xphnx/onze_cm11_theme/blob/HEAD/README.md
Source Code:https://gitlab.com/xphnx/onze_cm11_theme
Issue Tracker:https://gitlab.com/xphnx/onze_cm11_theme/issues

Auto Name:Onze
Summary:CM11 theme for devices using only FLOSS apps
Description:
A port of TwelF CM12 Theme. This theme contains exclusively
icons for apps hosted on F-Droid FLOSS Repository.

TwelF follows the guidelines of Google Material Design for Android Lollipop
aiming to provide a unified and minimalistic look at devices
using CM11 with only Libre or Open Source Apps.

For issues, comments and icon request, please use the issue tracker.

Status: Alpha

[https://gitlab.com/xphnx/onze_cm11_theme/blob/HEAD/CHANGELOG.md Changelog]
.

Repo Type:git
Repo:https://gitlab.com/xphnx/onze_cm11_theme.git

Build:0.4,4
    commit=ddf5077f179f2c1f6054cc6d15816019f3f184fd
    subdir=theme
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.4
Current Version Code:4
