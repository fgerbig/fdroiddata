Categories:Games
License:GPLv3
Web Site:
Source Code:https://gitlab.com/fgerbig/SpacePeng
Issue Tracker:https://gitlab.com/fgerbig/SpacePeng/issues

Name:SpacePeng!
Summary:A small but fun space shooter.
Description:
If you loved 'Space Invaders' or 'Phoenix' this game is for you.
How many levels can you beat in one session?
.

Repo Type:git
Repo:https://gitlab.com/fgerbig/SpacePeng.git

Build:V1.58-1,1581
     commit=dab1240fd64787196b02cee631a5c5d05c517663

Auto Update Mode:None
Update Check Mode:None
Current Version:1.58-1
